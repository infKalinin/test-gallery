const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');




const extractSass = new ExtractTextPlugin({
    filename: "style.css"
});





module.exports = {

    entry:'./src/main.js',

    output: {
        path:path.resolve(__dirname,"dist"),
        filename: "[name].js",// public path
    },

    module:{

        rules:[
            {
                test:/\.(pug|jade)$/,
                loader: ['html-loader', 'pug-html-loader'],

            },
            {
                test:  /\.scss$|\.sass$/,
                use: extractSass.extract({
                    use: [
                        { loader: 'css-loader', options: { sourceMap: true } },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    autoprefixer({
                                        browsers:['ie >= 8', 'last 4 version']
                                    })
                                ],
                                sourceMap: true
                            }
                        },
                        { loader: 'sass-loader', options: { sourceMap: true } }

                        ],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            {

                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }

            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                include: [
                    path.resolve(__dirname, "./src/img")
                ],
                use: [
                    {
                        loader:'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'img/'
                        }
                    },

                    {
                        loader: 'image-webpack-loader',

                    }

                ]
            },
            {
                test: /\.(eot|svg|woff|ttf|woff2)$/,
                include: [
                    path.resolve(__dirname, "./src/fonts")
                ],
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }

        ]

    },
    plugins: [
        new HtmlWebpackPlugin({
            template:'./src/index.html',
            title: 'My App'
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap:true,
            minimize: true,
            compress:{
                warnings:false
            }
        }),
        // new HtmlWebpackPlugin({
        //     filename: 'test.html',
        //     template: 'src/test.pug'
        // }),
        extractSass
    ],


};